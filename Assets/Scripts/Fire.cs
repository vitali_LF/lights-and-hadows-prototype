﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    public float Damage;
    private Health otherHealth;
    
    private void OnTriggerEnter(Collider other)
    {
        otherHealth = other.gameObject.GetComponent<Health>();
    }
    private void OnTriggerStay(Collider other)
    {
        if (otherHealth != null)
        {
            //Debug.Log("ON FIRE");
            otherHealth.MakeDamage(Damage);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        otherHealth = null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoop : MonoBehaviour
{
    void Start()
    {
        RestartGame();
    }

    public void RestartGame()
    {
        GameState.GetInstance().ChangeState(States.START);
    }

    public void EndGame()
    {
        GameState.GetInstance().ChangeState(States.GAME_OVER);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum States
{
    START,
    GAME_OVER,
    PAUSE,
    CONTINUE,
    UNDEFINED,
    FINISHED
}

public class GameState 
{
    private static GameState instance = null;
    private List<IGameStateListener> listeners = new List<IGameStateListener>();
    
    private States prevState;
    private States currentState;
    
    private GameState()
    {
        prevState = States.UNDEFINED;
        currentState = States.START;
    }
    
    public static GameState GetInstance()
    {
        return instance ?? (instance = new GameState());
    }
    
    public void ChangeState(States newState)
    {
        prevState = currentState;
        currentState = newState;
        OnStateChange();
    }
    
    public void AddListener(IGameStateListener listener)
    {
        if (!listeners.Contains(listener))
        {
            listeners.Add(listener);
        }
    }
    public void RemoveListener(IGameStateListener listener)
    {
        listeners.Remove(listener);
    }

    private void OnStateChange()
    {
        for (var i = listeners.Count - 1; i >= 0; i--) 
        {
            var listener = listeners[i];
            listener.OnStateChanged(prevState, currentState);
        }
    }

}

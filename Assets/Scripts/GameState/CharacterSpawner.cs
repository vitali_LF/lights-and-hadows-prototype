﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CharacterSpawner : MonoBehaviour, IGameStateListener
{
    public GameObject CharacterPrefab;
    public Transform StartPoint;
    public CinemachineVirtualCamera virtualCamera;
    
    private GameObject character;
    
    void Awake()
    {
        GameState.GetInstance().AddListener(this);
    }

    public void OnStateChanged(States oldState, States newState)
    {
        switch (newState)
        {
            case States.START:
                SpawnAtPoint(StartPoint.position);
                SetupCamera();
                break;
        }
    }

    private void SetupCamera()
    {
        virtualCamera.m_Follow = character.transform;
        //virtualCamera.m_LookAt = character.transform;
    }

    void SpawnAtPoint(Vector3 pos)
    {
        if (character != null)
        {
            Destroy(character);
        }
        character = Instantiate(CharacterPrefab, pos, Quaternion.identity);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSource : MonoBehaviour
{
    public bool isActive = true;
    [Tooltip("How big is radius of a light. A negative number means endless")]
    public float radius = -1f;

    void OnDrawGizmosSelected()
    {
        if (!isActive) return;
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }

    public void SetIsActive(bool newState)
    {
        isActive = newState;
    }
    
}

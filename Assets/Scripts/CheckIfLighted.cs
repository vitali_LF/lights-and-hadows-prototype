﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class CheckIfLighted : MonoBehaviour
{
    public LightSource[] lightSources = new LightSource[0];
    
    public UnityEvent gotLight;
    public UnityEvent lostLight;

    private bool lastStateUnderLight = false;
    
    public bool drawGizmos = false;
    
    
    void Start()
    {
        lightSources = FindObjectsOfType<LightSource>();
        if (lightSources.Length == 0)
        {
            Debug.LogError("Light sources not found");
            Destroy(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        bool underLight = false;
        foreach (var lightSource in lightSources)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, (lightSource.transform.position - transform.position), out hit,
                                lightSource.radius < 0 ? float.PositiveInfinity : lightSource.radius))
            {
                if (hit.collider.gameObject == lightSource.gameObject && lightSource.isActive)
                {
                    underLight = true;
//                    Debug.Log("Hit light + wtf " + hit.collider.gameObject.name);
                }
                else
                {
//                    Debug.Log("Hit shadow + wtf " + hit.collider.gameObject.name);
                }
            }
        }

        if (underLight != lastStateUnderLight)
        {
            
            if(underLight)
            {
                gotLight.Invoke();
            }
            else
            {
                lostLight.Invoke();
            }
        }
        
        lastStateUnderLight = underLight;
    }

    void OnDrawGizmosSelected()
    {
        if (!drawGizmos) return;
        
        Gizmos.DrawSphere(transform.position, 0.2f);

        foreach (var lightSource in lightSources)
        {
            if (lightSource.isActive) Gizmos.DrawLine(transform.position, lightSource.transform.position);
        }
    }
}

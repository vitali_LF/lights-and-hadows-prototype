﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class ChangeColor : MonoBehaviour
{
    public GameObject kira ; 
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void OnUnderLights()
    { 
        kira.GetComponent<Renderer>().material.mainTextureScale = new Vector2(14.06f, 1.0f);
    }
    
    public void OnInShadow()
    { 
        kira.GetComponent<Renderer>().material.mainTextureScale = new Vector2(6.74f, 1);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTwoTrigger : MonoBehaviour
{
    public bool oneInShadow = false;
    public bool twoInShadow = false;

    private bool wasShadow = false;

    public bool OneInShadow
    {
        get => oneInShadow;
        set => oneInShadow = value;
    }

    public bool TwoInShadow
    {
        get => twoInShadow;
        set => twoInShadow = value;
    }

    void Update()
    {
        if ((oneInShadow && twoInShadow) != wasShadow)
        {
            if (!(oneInShadow && twoInShadow)) this.transform.Translate(0, 3, 0);
            else this.transform.Translate(0, -3, 0);

        }
        wasShadow = (oneInShadow && twoInShadow);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverPanelSwitch : MonoBehaviour, IGameStateListener
{
    public States StateToGetEnabled;
    
    private void Awake()
    {
        GameState.GetInstance().AddListener(this);
        gameObject.SetActive(false);
    }
    
    public void OnStateChanged(States oldState, States newState)
    {
        gameObject.SetActive(newState == StateToGetEnabled);
    }
}

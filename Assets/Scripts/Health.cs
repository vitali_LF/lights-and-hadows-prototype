﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public float health = 100;
    
    public void MakeDamage(float damage)
    {
        health -= damage;
    }

    void Awake()
    {
        health = 100;
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
            GameState.GetInstance().ChangeState(States.GAME_OVER);
        }
    }
}

﻿using System;
using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour, IGameStateListener {

    public float speed = 3.0f;
    public float jumpness = 7.0f;
    public float distToGround = 0.5f;

    public bool IsMoving => !movementBlocked && rb.velocity != Vector3.zero;
    
    private Rigidbody rb;
    private Animator animator;
    private bool movementBlocked = false;
    private Vector3 prevPosition;

    void Awake()
    {
        GameState.GetInstance().AddListener(this);
        
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    private void OnDestroy()
    {
        GameState.GetInstance().RemoveListener(this);
    }

    void FixedUpdate ()
    {
        if (movementBlocked)
        {
            return;
        }

        prevPosition = transform.position;
        
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");
        float moveUp = rb.velocity.y;
    
        animator.SetBool("isGrounded", IsGrounded());
        
        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            moveUp = jumpness;
            //rb.velocity = Vector3.up * jumpness;
            animator.SetBool("isJumping", true);
        }

        Vector3 movement = new Vector3 (moveHorizontal *speed, moveUp, moveVertical * speed).normalized;
//        rb.AddForce(movement * speed);
        rb.velocity = movement;
        
        animator.SetFloat("walkingSpeed", moveHorizontal);
        animator.SetFloat("walkingSpeedAbs", Mathf.Max(Mathf.Abs(moveHorizontal), Mathf.Abs(moveVertical)));
    }

    private void StopMovementAnimation()
    {
        if (animator == null)
        {
            return;
        }
        animator.SetFloat("walkingSpeed", 0);
        animator.SetFloat("walkingSpeedAbs", 0);
    }

    bool IsGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, distToGround);
    }

    public void OnStateChanged(States oldState, States newState)
    {
        Debug.Log($"{newState} on PlayerController");
        switch (newState)
        {
            case States.UNDEFINED:
            case States.GAME_OVER:
            case States.FINISHED:
                movementBlocked = true;
                StopMovementAnimation();
                break;
            case States.START:
            case States.CONTINUE:
                movementBlocked = false;
                break;
        }
    }
}
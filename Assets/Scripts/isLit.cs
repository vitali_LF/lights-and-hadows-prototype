﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class isLit : MonoBehaviour
{
    public bool lit = false;

    public bool Lit
    {
        get => lit;
        set => lit = value;
    }

    private Color baseColor;

    void Start() {
        baseColor = gameObject.GetComponent<Renderer>().material.color;
    }

    void Update()
    {
        if (lit) gameObject.GetComponent<Renderer>().material.color = Color.white;
        else gameObject.GetComponent<Renderer>().material.color = baseColor;
    }
}

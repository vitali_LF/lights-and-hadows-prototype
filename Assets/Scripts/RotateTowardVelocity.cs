﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class RotateTowardVelocity : MonoBehaviour
{
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var dir = rb.velocity;
        dir.y = 0;
        if (dir.magnitude > 0.1)
            transform.rotation = Quaternion.LookRotation(dir);
    }
}

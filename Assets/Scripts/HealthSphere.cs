﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityTemplateProjects;

public class HealthSphere : MonoBehaviour
{
    public float health = 100;
    public float damage = 0.1f;
    public bool inShadow = false;
    public bool damageOnMove = false;
    public bool damageOnTime = true;

    public PlayerController playerController;
    
    public bool InShadow
    {
        get => inShadow;
        set => inShadow = value;
    }

    private MeshRenderer renderer;
    private MaterialPropertyBlock propertyBlock;
    
    private void MakeDamage(float damage)
    {
        health -= damage;
        
        propertyBlock.SetFloat("_Amount", 1 - (health/100));
        renderer.SetPropertyBlock(propertyBlock);
    }
    
    private void RestoreDamage(float damage)
    {
        health += damage;
        health = Mathf.Min(100, health);
        
        propertyBlock.SetFloat("_Amount", 1 - (health/100));
        renderer.SetPropertyBlock(propertyBlock);
    }

    void Awake()
    {
        renderer = GetComponent<MeshRenderer>();
        propertyBlock = new MaterialPropertyBlock();
        health = 100;
    }

    // Update is called once per frame
    void Update()
    {
        if (inShadow && playerController.IsMoving && damageOnMove)
        {
            MakeDamage(damage);
        }
        if (inShadow && damageOnTime)
        {
            MakeDamage(damage);
        }
        if (!inShadow)
        {
            RestoreDamage(damage * 2);
        }
        if (health <= 0)
        {
            Destroy(gameObject);
            GameState.GetInstance().ChangeState(States.GAME_OVER);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlatformEntered : MonoBehaviour
{
    
    Animator animator;
    public UnityEvent onEnter;
    
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("entered");
        animator.SetTrigger("PlayerEnters");
        onEnter.Invoke();
    }

    private void OnTriggerStay(Collider other)
    {
        Debug.Log("Stay");
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("exit");
        animator.SetTrigger("PlayerLeaves");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
